<?php

namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends AbstractController
{
    /**
     * @Route(
     *     "/",
     *     name="agenda_pages_inicio",
     *     methods={"GET", "POST"}
     * )
     */
    public function inicio()
    {
        $nombre = 'Álex';
        return $this->render(
            'pages/inicio.html.twig',
            [
                'nombre' => $nombre
            ]
            );
    }
}