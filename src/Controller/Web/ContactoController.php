<?php

namespace App\Controller\Web;

use App\BLL\ContactoBLL;
use App\Entity\Contacto;
use App\Form\ContactoType;
use App\Helper\FileUploader;
use App\Repository\ContactoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class ContactoController extends AbstractController
{
    /**
     * @Route(
     *     "/contactos/guarda",
     *     name="agenda_contacto_guarda",
     *     methods={"POST"}
     * )
     */
    public function guarda(Request $request, ContactoBLL $contactoBLL)
    {
        $contacto = new Contacto();

        $form = $this->createForm(
            ContactoType::class,
            $contacto,
            [
                'action' => $this->generateUrl("agenda_contacto_guarda")
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $imgFoto = $form->get('fotoFile')->getData();

            $contactoBLL->guarda($contacto, $imgFoto);

            $this->addFlash(
                'notice',
                'El contacto se ha guardado correctamente!'
            );

            return $this->redirectToRoute('agenda_contacto_listar');
        }

        return $this->render(
            'contacto/nuevo.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(
     *     "/contactos/nuevo",
     *     name="agenda_contacto_nuevo",
     *     methods={"GET"}
     * )
     */
    public function nuevo(Request $request, ContactoBLL $contactoBLL)
    {
        $contacto = new Contacto();

        $form = $this->createForm(
            ContactoType::class,
            $contacto,
            [
                'action' => $this->generateUrl("agenda_contacto_guarda")
            ]
        );

        return $this->render(
            'contacto/nuevo.html.twig',
            [
                'contactos' => $this->getDoctrine()->getRepository(Contacto::class)->findAll(),
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(
     *     "/contactos/{id}/actualizar",
     *     name="agenda_contacto_actualiza",
     *     methods={"POST"}
     * )
     */
    public function actualiza(Request $request, ContactoBLL $contactoBLL, Contacto $contacto)
    {
        $form = $this->createForm(
            ContactoType::class,
            $contacto,
            [
                'action' => $this->generateUrl("agenda_contacto_actualiza", [ 'id' => $contacto->getId() ] )
            ]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $imgFoto = $form->get('fotoFile')->getData();

            $contactoBLL->guarda($contacto, $imgFoto);

            $this->addFlash(
                'notice',
                'El contacto se ha guardado correctamente!'
            );

            return $this->redirectToRoute('agenda_contacto_listar');
        }

        return $this->render(
            'contacto/editar.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(
     *     "/contactos/{id}/editar",
     *     name="agenda_contacto_editar",
     *     methods={"GET"}
     * )
     */
    public function editar(Request $request, ContactoBLL $contactoBLL, Contacto $contacto)
    {
        $form = $this->createForm(
            ContactoType::class,
            $contacto,
            [
                'action' => $this->generateUrl("agenda_contacto_actualiza", [ 'id' => $contacto->getId() ] )
            ]
        );

        return $this->render(
            'contacto/editar.html.twig',
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route(
     *     "/contactos",
     *     name="agenda_contacto_listar",
     *     methods={"GET", "POST"}
     * )
     */
    public function listar(Request $request, ContactoBLL $contactoBLL)
    {
        return $this->render(
            'contacto/listar.html.twig',
            [
                'contactos' => $this->getDoctrine()->getRepository(Contacto::class)->findAll()
            ]
        );
    }

    /**
     * @Route(
     *     "/contactos/{id}/delete",
     *     name="agenda_contacto_eliminar",
     *     methods={"GET"},
     *     requirements={"id"="\d+"}
     * )
     */
    public function eliminar(Request $request, ContactoBLL $contactoBLL, Contacto $contacto)
    {
        $id = $contacto->getId();

        $contactoBLL->eliminar($contacto);

        $this->addFlash(
            'notice',
            "El contacto $id se ha eliminado correctamente!"
        );

        return $this->redirectToRoute('agenda_contacto_listar');
    }

    /**
     * @Route(
     *     "/contactos/buscar",
     *     name="agenda_contacto_buscar",
     *     methods={"POST"}
     * )
     */
    public function buscar(Request $request, ContactoBLL $contactoBLL)
    {
        $busqueda = $request->request->get('search');

        /** @var ContactoRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Contacto::class);

        $contacto = $repository->find(intval($busqueda));

        if (is_null($contacto))
            $contactos = $repository->findContacto($busqueda);
        else
            $contactos = [ $contacto ];

        return $this->render(
            'contacto/listar.html.twig',
            [
                'contactos' => $contactos
            ]
        );
    }
}