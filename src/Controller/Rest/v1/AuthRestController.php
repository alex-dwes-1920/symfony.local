<?php


namespace App\Controller\Rest\v1;

use App\BLL\UsuarioBLL;
use App\Controller\Rest\BaseApiController;
use App\Entity\Usuario;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class AuthRestController extends BaseApiController
{
    /**
     * @Route("/auth/login")
     */
    public function getTokenAction()
    {
        // The security layer will intercept this request
        return new Response('', Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @Route(
     *     "/auth/register.{_format}", name="agenda_apiv1_register",
     *     requirements={
     *          "_format": "json"
     *     },
     *     defaults={"_format": "json"},
     *     methods={"POST"}
     * )
     */
    public function register(Request $request, UsuarioBLL $usuarioBLL)
    {
        $data = $this->getContent($request);

        $user = $usuarioBLL->nuevo($data['username'], $data['password']);

        return $this->getResponse($user, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *     "/auth/login/google.{_format}",
     *     name="agenda_apiv1_login_google",
     *     requirements={
     *          "_format": "json"
     *     },
     *     defaults={"_format": "json"}
     * )
     */
    public function googleLogin(Request $request, UsuarioBLL $userBLL)
    {
        $data = $this->getContent($request);

        if (is_null($data['access_token'])
            || !isset($data['access_token'])
            || empty($data['access_token']))
            throw new BadRequestHttpException('No se ha recibido el token de google');

        $googleJwt = json_decode(file_get_contents(
            "https://www.googleapis.com/plus/v1/people/me?access_token=" .
            $data['access_token']));

        $token = $userBLL->getTokenByEmail($googleJwt->emails[0]->value);

        return $this->getResponse(['token' => $token]);
    }
}