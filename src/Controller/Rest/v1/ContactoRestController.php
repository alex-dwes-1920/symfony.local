<?php


namespace App\Controller\Rest\v1;

use App\BLL\ContactoBLL;
use App\Controller\Rest\BaseApiController;
use App\Entity\Contacto;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactoRestController extends BaseApiController
{
    /**
     * @Route(
     *     "/contactos.{_format}",
     *     name="agenda_apiv1_get_contactos",
     *     methods={"GET"}
     * )
     * @Route("/contactos/ordenados/{order}", name="agenda_apiv1_get_contactos_ordenados")
     */
    public function getAll(Request $request, ContactoBLL $contactoBLL, string $order='nombre')
    {
        $nombre = $request->query->get('nombre');
        $telefono = $request->query->get('telefono');

        $result = $contactoBLL->getContactosFiltrados($order, $nombre, $telefono);

        return $this->getResponse($result);
    }

    /**
     * @Route(
     *     "/contactos/{id}.{_format}",
     *     name="agenda_apiv1_get_contacto",
     *     requirements={
     *          "id": "\d+"
     *     },
     *     methods={"GET"}
     * )
     */
    public function getOne(Contacto $contacto, ContactoBLL $contactoBLL)
    {
        return $this->getResponse($contactoBLL->toArray($contacto));
    }

    /**
     * @Route(
     *     "/contactos.{_format}",
     *     name="agenda_apiv1_post_contactos",
     *     methods={"POST"}
     * )
     */
    public function post(Request $request, ContactoBLL $contactoBLL)
    {
        $data = $this->getContent($request);

        $contacto = $contactoBLL->nuevo($request, $data);

        return $this->getResponse($contacto, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *     "/contactos/{id}.{_format}",
     *     name="agenda_apiv1_put_contactos",
     *     requirements={
     *          "id": "\d+"
     *     },
     *     methods={"PUT"}
     * )
     */
    public function update(Request $request, Contacto $contacto, ContactoBLL $contactoBLL)
    {
        $data = $this->getContent($request);

        $contacto = $contactoBLL->update($contacto, $data);

        return $this->getResponse($contacto, Response::HTTP_OK);
    }

    /**
     * @Route(
     *     "/contactos/{id}.{_format}",
     *     name="agenda_apiv1_delete_contactos",
     *     requirements={
     *          "id": "\d+"
     *     },
     *     methods={"DELETE"}
     * )
     */
    public function delete(Contacto $contacto, ContactoBLL $contactoBLL)
    {
        $contactoBLL->delete($contacto);

        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }
}