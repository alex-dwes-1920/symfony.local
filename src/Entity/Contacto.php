<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactoRepository")
 */
class Contacto
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El campo nombre no puede quedar vacío")
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="El campo teléfono no puede quedar vacío")
     * @Assert\Length(
     *      min = 9,
     *      max = 9,
     *      exactMessage = "El teléfono debe tener {{ limit }} dígitos"
     * )
     * @Assert\Regex("/^\d+/", message="El teléfono solo puede contener números")
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $foto;

    /**
     * @var UploadedFile
     * @Assert\Image(
     *     mimeTypes={ "image/png", "image/jpeg" },
     *     mimeTypesMessage="Tipo de imagen no permitido, solo se permite PNG y JPG"
     * )
     */
    private $fotoFile;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    private $fechaAlta;

    /**
     * @var Usuario
     * @ORM\ManyToOne(targetEntity="App\Entity\Usuario")
     */
    private $usuario;

    /**
     * Contacto constructor.
     */
    public function __construct()
    {
        $this->fechaAlta = new \DateTime();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTelefono(): ?string
    {
        return $this->telefono;
    }

    public function setTelefono(string $telefono): self
    {
        $this->telefono = $telefono;

        return $this;
    }

    public function getFoto()
    {
        return $this->foto;
    }

    public function setFoto($foto): self
    {
        $this->foto = $foto;

        return $this;
    }

    public function getFechaAlta(): ?\DateTimeInterface
    {
        return $this->fechaAlta;
    }

    public function setFechaAlta(\DateTimeInterface $fechaAlta): self
    {
        $this->fechaAlta = $fechaAlta;

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getFotoFile(): ?UploadedFile
    {
        return $this->fotoFile;
    }

    /**
     * @param UploadedFile $fotoFile
     * @return Contacto
     */
    public function setFotoFile(UploadedFile $fotoFile): Contacto
    {
        $this->fotoFile = $fotoFile;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getUsuario(): Usuario
    {
        return $this->usuario;
    }

    /**
     * @param Usuario $usuario
     * @return Contacto
     */
    public function setUsuario(Usuario $usuario): Contacto
    {
        $this->usuario = $usuario;
        return $this;
    }
}
