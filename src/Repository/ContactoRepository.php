<?php

namespace App\Repository;

use App\Entity\Contacto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Contacto|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contacto|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contacto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contacto::class);
    }

    public function findAll()
    {
        $qb = $this->createQueryBuilder('contacto');

        $qb ->addSelect('usuario')
            ->innerJoin('contacto.usuario', 'usuario');
//            ->where($qb->expr()->eq('usuario.username', ':username'))
//            ->setParameter('username', 'alex');

        return $qb->getQuery()->execute();
    }

    public function findContacto(string $search)
    {
        $qb = $this->createQueryBuilder('contacto');

        $qb->where(
            $qb->expr()->orX(
                $qb->expr()->like('contacto.nombre', ':search'),
                $qb->expr()->like('contacto.telefono', ':search')
            )
        );
//        $qb->where($qb->expr()->like('contacto.nombre', ':search'));
//        $qb->orWhere($qb->expr()->like('contacto.telefono', ':search'));
        $qb->setParameter('search', '%' . $search . '%');

        return $qb->getQuery()->execute();
    }

    public function getContactosFiltrados(
        string $order, int $idUsuario, string $nombre=null, string $telefono=null)
    {
        $qb = $this ->createQueryBuilder('contacto');

        $qb->where($qb->expr()->eq('contacto.usuario', ':usuario'))
            ->setParameter('usuario', $idUsuario);

        if (isset($nombre))
        {
            $qb->andWhere($qb->expr()->like('contacto.nombre', ':nombre'))
                ->setParameter('nombre', '%'.$nombre.'%');
        }

        if (isset($telefono))
        {
            $qb->andWhere($qb->expr()->like('contacto.telefono', ':telefono'))
                ->setParameter('telefono', '%'.$telefono.'%');
        }

        $qb->orderBy('contacto.'.$order, 'ASC');

        return $qb->getQuery()->getResult();
    }

    // /**
    //  * @return Contacto[] Returns an array of Contacto objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contacto
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
