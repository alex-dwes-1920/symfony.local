<?php

namespace App\BLL;

use App\Entity\Contacto;
use App\Entity\Usuario;
use App\Helper\FileUploader;
use App\Repository\ContactoRepository;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class ContactoBLL extends BaseBLL
{
    private $uploader;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function eliminar(Contacto $contacto)
    {
        $this->entityManager->remove($contacto);
        $this->entityManager->flush();
    }

    public function guarda(Contacto $contacto, UploadedFile $imgFoto=null)
    {
        if (!is_null($imgFoto))
        {
            $fileName = $this->uploader->upload($imgFoto);

            $contacto->setFoto($fileName);
        }

        $this->entityManager->persist($contacto);
        $this->entityManager->flush();

        return $contacto;
    }

    /**
     * @param Request $request
     * @param array $data
     * @return array
     * @throws \Exception
     */
    public function nuevo(Request $request, array $data)
    {
        if (!isset($data['nombre']) || !isset($data['telefono']))
            throw new BadRequestHttpException('No se ha recibido el nombre y/o el teléfono');

        $contacto = new Contacto();
        $contacto->setNombre($data['nombre']);
        $contacto->setTelefono($data['telefono']);

        $usuario = $this->getUser();
        $contacto->setUsuario($usuario);

        if (isset($data['foto']) && !empty($data['foto']))
        {
            $fotos_upload_dir = $this->params->get('fotos_upload_dir');
            $fotos_url_directory = $this->params->get('fotos_url_directory');

            $arr_foto = explode(',', $data['foto']);
            if (count($arr_foto) < 2)
                throw new BadRequestHttpException('formato de imagen incorrecto');

            $imgFoto = base64_decode($arr_foto[1]);
            if (!is_null($imgFoto))
            {
                $fileName = md5(uniqid()).'.png';
                $filePath = $fotos_url_directory . $fileName;
                $urlAvatar = $request->getUriForPath($filePath);
                $contacto->setFoto($fileName);

                $ifp = fopen($fotos_upload_dir . '/' . $fileName, "wb");
                if ($ifp)
                {
                    $ok = fwrite($ifp, $imgFoto);
                    if (!$ok)
                        throw new \Exception('No se ha podido cargar la imagen del avatar');

                    fclose($ifp);
                }
            }
            else
                throw new \Exception('No se ha podido cargar la imagen del avatar');
        }

        return $this->guardaValidando($contacto);
    }

    public function getContactosFiltrados(
        string $order, string $nombre=null, string $telefono=null)
    {
        $usuario = $this->getUser();

        /** @var ContactoRepository $contactoRepository */
        $contactoRepository = $this->entityManager->getRepository(Contacto::class);

        $contactos = $contactoRepository->getContactosFiltrados(
            $order, $usuario->getId(), $nombre, $telefono);

        return $this->entitiesToArray($contactos);
    }

    public function getAll()
    {
        $contactos = $this->entityManager->getRepository(Contacto::class)->findAll();

        return $this->entitiesToArray($contactos);
    }

    public function update(Contacto $contacto, array $data)
    {
        $contacto->setNombre($data['nombre']);
        $contacto->setTelefono($data['telefono']);

        return $this->guardaValidando($contacto);
    }

    public function toArray($contacto) : array
    {
        return [
            'id' => $contacto->getId(),
            'nombre' => $contacto->getNombre(),
            'telefono' => $contacto->getTelefono()
        ];
    }
}