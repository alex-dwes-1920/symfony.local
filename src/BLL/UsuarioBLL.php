<?php


namespace App\BLL;

use App\Entity\Usuario;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsuarioBLL extends BaseBLL
{
    /** @var UserPasswordEncoderInterface $encoder */
    private $encoder;

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    /**
     * @required
     */
    public function setJWTManager(JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }

    /**
     * @required
     */
    public function setEncoder(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function getAll() : array
    {
        $usuarios = $this->entityManager->getRepository(Usuario::class)->findAll();

        return $this->entitiesToArray($usuarios);
    }

    public function nuevo(string $username, string $password) : array
    {
        $user = new Usuario();

        $user->setUsername($username);
        $user->setPassword($this->encoder->encodePassword($user, $password));

        return $this->guardaValidando($user);
    }

    public function profile() : array
    {
        $user = $this->getUser();

        return $this->toArray($user);
    }

    public function cambiaPassword(string $nuevoPassword) : array
    {
        $user = $this->getUser();

        $user->setPassword($this->encoder->encodePassword($user, $nuevoPassword));

        return $this->guardaValidando($user);
    }

    public function toArray($usuario) : array
    {
        return [
            'id' => $usuario->getId(),
            'username' => $usuario->getUsername(),
            'role' => $usuario->getRole(),
            'active' => $usuario->getIsActive()
        ];
    }

    public function getTokenByEmail(string $email)
    {
        $usuario = $this->entityManager->getRepository(Usuario::class)
            ->findOneBy(array('email'=>$email));

        if (is_null($usuario))
            throw new AccessDeniedHttpException('Usuario no autorizado');

        return $this->jwtManager->create($usuario);
    }
}